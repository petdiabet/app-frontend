import 'package:flutter/material.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';
import 'package:pet_diabet/widgets/or_divider.dart';
import 'package:pet_diabet/widgets/social_icon.dart';
import 'package:pet_diabet/widgets/background.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
        //child: SingleChildScrollView(
        child: Scaffold(
            backgroundColor: Colors.lightBlue[200],
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: size.height * 0.10),

                  Image.asset(
                    "assets/images/icone.png",
                    fit: BoxFit.contain,
                    height: size.height * 0.35,
                  ),

                  //SizedBox(height: size.height * 0.05),
                  RoundedButton(
                    text: "LOGIN",
                    textColor: Colors.lightBlue[700],
                    bord: Colors.white,
                    press: () {
                      Navigator.pushNamed(context, '/login');
                    },
                  ),

                  RoundedButton(
                    //text: "SIGN UP",
                    text: "CADASTRAR",
                    bord: Colors.lightBlue[700],
                    press: () {
                      Navigator.pushNamed(context, '/signup');
                    },
                  ),

                  //GridDashboard(),
                  OrDivider(texto: "Siga nossas Redes Sociais"),

                  SizedBox(height: size.height * 0.02),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SocalIcon(
                        iconSrc: "assets/icons/facebook.svg",
                        press: () {},
                        backgroundBorder: Colors.white,
                        svgColor: Colors.lightBlue[900],
                      ),
                      SocalIcon(
                        iconSrc: "assets/icons/twitter.svg",
                        press: () {},
                        backgroundBorder: Colors.white,
                        svgColor: Colors.lightBlue[700],
                      ),
                      SocalIcon(
                        iconSrc: "assets/icons/instagram.svg",
                        press: () {},
                        backgroundBorder: Colors.white,
                        //svgColor: Colors.lightBlue[700],
                      ),
                    ],
                  ),
                  SizedBox(height: size.height * 0.05),
                  Footer(),
                  SizedBox(height: size.height * 0.04),
                ],
              ),
            )));
  }
}
