import 'package:flutter/material.dart';
import 'package:pet_diabet/view/choose_pets/components/character_listing_screen.dart';

class OptionsPet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.blue,
      body: CharacterListingScreen(),
      appBar: buildAppBar(context),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.more_vert,
            // By default our  icon color is white
            color: Colors.black,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
}
