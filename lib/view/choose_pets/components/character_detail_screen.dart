/*
*     @author: Jefferson Gomes
*    
*     Descricao:
*       Tela com informacoes dos pets disponiveis pelo aplicativo.
*/

import 'package:after_layout/after_layout.dart';
import 'package:pet_diabet/models/character.dart';
import 'package:flutter/material.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';
import 'package:pet_diabet/view/home_page/home.dart';

class CharacterDetailScreen extends StatefulWidget {
  final double _collapsedBottomSheetBottomPosition = -250;
  final double _completeCollapsedBottomSheetBottomPosition = -330;
  final Character character;

  const CharacterDetailScreen({Key key, this.character}) : super(key: key);

  @override
  _CharacterDetailScreenState createState() => _CharacterDetailScreenState();
}

class _CharacterDetailScreenState extends State<CharacterDetailScreen>
    with AfterLayoutMixin<CharacterDetailScreen> {
  double _bottomSheetBottomPosition = -330;
  bool isCollapsed = false;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Hero(
            tag: "background-${widget.character.name}",
            child: DecoratedBox(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: widget.character.colors,
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, left: 16),
                  child: IconButton(
                    iconSize: 40,
                    icon: Icon(Icons.close),
                    color: Colors.white.withOpacity(0.9),
                    onPressed: () {
                      setState(() {
                        _bottomSheetBottomPosition =
                            widget._completeCollapsedBottomSheetBottomPosition;
                      });
                      Navigator.pop(context);
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Hero(
                      tag: "image-${widget.character.name}",
                      child: Image.asset(widget.character.imagePath,
                          height: screenHeight * 0.45)),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32.0, vertical: 2),
                  /*
                    @TODO:
                      RETIRADO O NOME DO PET

                  child: Hero(
                    tag: "name-${widget.character.name}",
                      
                    child: Material(
                      color: Colors.transparent,
                      
                      child: Container(
                            
                        child: Text(widget.character.name,
                          style: AppTheme.heading
                        )
                      )
                    )
                  ),
                  */
                ),

                //@TODO:
                //  RETIRANDO A DESCRICAO DO PET

                Padding(
                  padding: const EdgeInsets.fromLTRB(32, 0, 8, 32),
                  child: Text(
                    widget.character.description,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontFamily: 'WorkSans-Bold',
                    ),
                  ),
                ),

                Align(
                    alignment: Alignment.center,
                    child: RoundedButton(
                      text: "ESCOLHER",
                      textColor: Colors.lightBlue[700],
                      bord: Colors.white,
                      press: () {
                        _eventRegister(context);
                      },
                    )),
              ],
            ),
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 500),
            curve: Curves.decelerate,
            bottom: _bottomSheetBottomPosition,
            left: 0,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

/*
  Widget roundedContainer(Color color) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
    );
  }
*/
  @override
  void afterFirstLayout(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        isCollapsed = true;
        _bottomSheetBottomPosition = widget._collapsedBottomSheetBottomPosition;
      });
    });
  }

  _eventRegister(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return HomeGamer();
        },
      ),
    );
  }
}
