/*
*     @author: Jefferson Gomes
*    
*     Descricao:
*       Tela de escolha dos pets disponiveis pelo aplicativo, a ideia é mostrar
*       os pets e uma breve descricao de cada pet, para o jogador decidir qual
*       na opinião dele é o melhor para iniciar.
*/

import 'package:pet_diabet/models/character.dart';
import 'package:pet_diabet/config/styleguide.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/view/choose_pets/components/character_widget.dart';
import 'package:flutter/material.dart';
import 'package:pet_diabet/config/constants.dart';

class CharacterListingScreen extends StatefulWidget {
  @override
  _CharacterListingScreenState createState() => _CharacterListingScreenState();
}

class _CharacterListingScreenState extends State<CharacterListingScreen> {
  PageController _pageController;
  int currentPage = 0, pages = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      viewportFraction: 1.0,
      initialPage: currentPage,
      keepPage: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 13.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // --> Titulo

              Padding(
                padding: const EdgeInsets.only(left: 32.0, top: 15.0),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                          text: "Escolha seu Pet", style: AppTheme.display1),
                      TextSpan(text: "\n"),
                    ],
                  ),
                ),
              ),

              // --> A imagem dos pets na tela

              Expanded(
                flex: 3,
                child: PageView.builder(
                  physics: ClampingScrollPhysics(),
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemCount: characters.length,
                  controller: _pageController,
                  itemBuilder: (context, index) => CharacterWidget(
                      character: characters[index],
                      pageController: _pageController,
                      currentPage: index),
                ),
              ),

              // --> Botão e o rodapé

              Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.02),
                  child: Column(
                    children: <Widget>[
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          characters.length,
                          (index) => buildDot(index: index),
                        ),
                      ),
                      Spacer(flex: 2),

                      // @TODO: TEXTO INFORMANDO AO USUARIO QUE DEVE CLICAR NO PET
                      // PARA ESCOLHE-LO
                      Container(
                        child: Text(
                          "Clique no Pet para escolher!",
                          style: TextStyle(
                            fontSize: screenWidth * 0.05,
                          ),
                        ),
                      ),
                      Spacer(),
                      Footer()
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
