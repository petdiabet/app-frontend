import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet_diabet/widgets/background.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';
import 'package:pet_diabet/widgets/rounded_input.dart';
import 'package:pet_diabet/widgets/or_divider.dart';

/*

  --> Cadastro de Refeição e Tipo de refeição

*/

class MealRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[200],
      //backgroundColor: Color.fromARGB(255, 148,0,211),
      body: form(context),
    );
  }

  Widget form(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.05),
            Image.asset(
              "assets/images/icone.png",
              height: size.height * 0.30,
              //width: size.width * 1.20,
            ),
            OrDivider(
              texto: ".: REFEIÇÃO :.",
            ),
            RoundedInputField(
              hintText: "Nome da Refeição",
              icon: Icons.list,
              // myController: this._nome,
            ),
            RoundedInputField(
              hintText: "Descrição",
              icon: Icons.add,
              // myController: this._cpf,
            ),
            SizedBox(height: size.height * 0.04),
            OrDivider(
              texto: ".: TIPO DE REFEIÇÃO :.",
            ),
            RoundedInputField(
              hintText: "Nome",
              icon: Icons.list,
              // myController: this._nome,
            ),
            RoundedInputField(
              hintText: "Descrição",
              icon: Icons.add,
              // myController: this._cpf,
            ),
            RoundedButton(
              text: "CADASTRAR",
              bord: Colors.lightBlue[700],
              textColor: Colors.white,
              press: () {
                showDialog(
                    context: context,
                    builder: (_) => CupertinoAlertDialog(
                          title: Text("Você Confirma?"),
                          actions: <Widget>[
                            CupertinoDialogAction(
                                textStyle: TextStyle(color: Colors.red),
                                isDefaultAction: true,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text("NAO")),
                            CupertinoDialogAction(
                                isDefaultAction: true,
                                onPressed: () async {
                                  //_eventRegister(context);
                                },
                                child: Text("SIM")),
                          ],
                        ));
              },
            ),
            SizedBox(height: size.height * 0.05),
            Footer(),
            SizedBox(height: size.height * 0.02),
          ],
        ),
      ),
    );
  }
}
