import 'package:flutter/material.dart';
import 'package:pet_diabet/widgets/background.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.lightBlue[100],
        appBar: buildAppBar(context),
        body: form(context));
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.lightBlue[200],
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          Icons.keyboard_backspace,
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.more_vert,
            // By default our  icon color is white
            color: Colors.white,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  Widget form(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/icone.png",
              height: size.height * 0.35,
            ),
            RoundedButton(
              text: "JOGADOR",
              bord: Colors.lightBlue[700],
              press: () => Navigator.pushNamed(context, '/register_jogador'),
            ),
            RoundedButton(
              bord: Colors.white,
              textColor: Colors.lightBlue[700],
              text: "RESPONSAVEL",
              press: () => Navigator.pushNamed(context, '/register'),
            ),
            RoundedButton(
              bord: Colors.lightBlue[700],
              text: "MEDICO",
              press: () => Navigator.pushNamed(context, '/register_medico'),
            ),
            SizedBox(height: size.height * 0.05),
            Footer(),
          ],
        ),
      ),
    );
  }
}
