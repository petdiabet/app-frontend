import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet_diabet/view/login/login.dart';
import 'package:pet_diabet/widgets/background.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/widgets/have_account.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';
import 'package:pet_diabet/widgets/rounded_input.dart';
import 'package:pet_diabet/widgets/password_field.dart';

class Register extends StatelessWidget {
  final _nome = TextEditingController();
  final _cpf = TextEditingController();
  final _email = TextEditingController();
  final _senha = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[100],
      //backgroundColor: Color.fromARGB(255, 148,0,211),
      body: form(context),
    );
  }

  Widget form(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.08),
            Image.asset(
              "assets/images/icone.png",
              height: size.height * 0.30,
              //width: size.width * 1.20,
            ),
            RoundedInputField(
              hintText: "Nome",
              icon: Icons.person,
              myController: this._nome,
            ),
            RoundedInputField(
              hintText: "CPF",
              icon: Icons.rate_review,
              myController: this._cpf,
            ),
            RoundedInputField(
              hintText: "Email",
              icon: Icons.email,
              myController: this._email,
            ),
/*
          DropdownButton<String>(
            items: [
              DropdownMenuItem<String>(
                child: Row(
                    children: <Widget>[
                      Icon(Icons.filter_1),
                      Text('Item 1'),
                    ],
                ),
                value: 'one',
              ),
            ],
          ),  
*/
            RoundedInputField(
              hintText: "Sexo",
              icon: Icons.person,
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              myController: this._senha,
            ),
            RoundedButton(
              text: "CADASTRAR",
              bord: Colors.lightBlue[700],
              textColor: Colors.white,
              press: () {
                print('opssss');
                Navigator.pushNamed(context, '/pets');
                /*
                showDialog(
                    context: context,
                    builder: (_) => CupertinoAlertDialog(
                          title: Text("Você Confirma?"),
                          actions: <Widget>[
                            CupertinoDialogAction(
                                textStyle: TextStyle(color: Colors.red),
                                isDefaultAction: true,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text("NAO")),
                            CupertinoDialogAction(
                                isDefaultAction: false,
                                onPressed: () {
                                  //_eventRegister(context);
                                  print('OPA SIM');
                                  Navigator.pushNamed(context, '/pets');
                                },
                                child: Text("SIM")),
                          ],
                        ));
                  */
              },
            ),
            SizedBox(height: size.height * 0.02),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            SizedBox(height: size.height * 0.05),
            Footer(),
            SizedBox(height: size.height * 0.02),
          ],
        ),
      ),
    );
  }

  _eventRegister(context) {
    print("Hora Hacker do Jeff: \n\n" +
        "\nNome: " +
        this._nome.text +
        "\nCPF: " +
        this._cpf.text +
        "\nEmail: " +
        this._email.text +
        "\nSenha: " +
        this._senha.text.toString());

    Navigator.pushNamed(context, '/pets');
  }
}
