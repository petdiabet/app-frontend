import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:pet_diabet/widgets/background.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        //appBar: buildAppBar(context),

        bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.blueAccent,
          items: <Widget>[
            Icon(Icons.add, size: 30),
            Icon(Icons.list, size: 30),
            Icon(Icons.compare_arrows, size: 30),
          ],
          onTap: (index) {
            //Handle button tap
          },
        ),
        body: Background(
            child: Stack(children: <Widget>[
          Center(
              child: Image.asset(
            "assets/images/pets/01_1.png",
            height: screenHeight - 100,
            width: screenWidth - 100,
          )),

          //SideBar(),
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  alignment: Alignment.topCenter,
                  icon: Image.asset(
                    "assets/images/notification.png",
                    width: 24,
                  ),
                  onPressed: () {},
                )
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
        ])));
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.lightBlue[100],
      elevation: 0,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.monetization_on,
            // By default our  icon color is white
            color: Colors.yellow,
          ),
          onPressed: () {},
        ),
        Text('0')
      ],
    );
  }
}
