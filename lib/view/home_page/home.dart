import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:pet_diabet/widgets/social_icon.dart';
import 'package:pet_diabet/widgets/or_divider.dart';
import 'package:pet_diabet/view/meal/meal_register.dart';
import 'package:pet_diabet/view/food_registration/food_registration.dart';

class HomeGamer extends StatefulWidget {
  @override
  HomeState createState() => new HomeState();
}

class HomeState extends State<HomeGamer> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.lightBlue[100],
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.blueAccent,
        items: <Widget>[
          Icon(Icons.add, size: 30),
          Image.asset(
            "assets/images/food.png",
            height: 25,
          ),
          Icon(Icons.list, size: 30),
          Image.asset(
            "assets/images/gamepad-controller.png",
            height: 25,
          ),
          Icon(Icons.settings, size: 30),
        ],
        onTap: (index) async {
          print(index.toString());
          _verificarOpcao(index, context);
        },
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 60,
          ),
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Usuario: Jefferson",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      "Nome do Pet: Rockbush",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Color(0xffa29aac),
                              fontSize: 14,
                              fontWeight: FontWeight.w600)),
                    ),
                  ],
                ),
                Expanded(
                    flex: 3,
                    child: IconButton(
                      alignment: Alignment.topRight,
                      icon: Image.asset(
                        "assets/images/dollar.png",
                        width: 24,
                      ),
                      onPressed: () {},
                    )),
                Expanded(
                    flex: 1,
                    child: Text(
                      "0",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Color(0xffa29aac),
                              fontSize: 14,
                              fontWeight: FontWeight.w600)),
                    )),
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            //mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                "Humor: Alegre",
                style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.bold)),
              ),
              Text(
                "Motivo: ----",
                style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        color: Color(0xffa29aac),
                        fontSize: 14,
                        fontWeight: FontWeight.w600)),
              ),
              Image.asset(
                "assets/images/pets/01_1.png",

                height: screenHeight - 450,
                //width: 100,
              ),
              OrDivider(
                texto: ".: CUIDAR DO PET :.",
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SocalIcon(
                    iconSrc: "assets/icons/banho2.svg",
                    press: () {},
                    backgroundBorder: Colors.white,
                    //svgColor: Colors.lightBlue[900],
                  ),
                  SocalIcon(
                    iconSrc: "assets/icons/food2.svg",
                    press: () {},
                    backgroundBorder: Colors.white,
                    svgColor: Colors.red,
                  ),
                ],
              ),
            ],
          ),

          /* Column(

            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,

            children: <Widget> [
            RoundedButton(
              text: "JOGAR",
              bord: Colors.lightBlue[700],
              press: () {
                Navigator.pushNamed(context, '/');
              },
            ),
            //SizedBox(height: 1),
            ],
            ),*/
        ],
      ),
    );
  }

  _verificarOpcao(index, context) async {
    switch (index.toString()) {
      case "0":
        return Navigator.pushNamed(context, '/meal_register');
        break;

      case "1":
        return Navigator.pushNamed(context, '/food_register');
        break;
    }
  }
}
