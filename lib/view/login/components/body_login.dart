import 'package:flutter/material.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'package:pet_diabet/widgets/background.dart';
import 'package:pet_diabet/widgets/rounded_button.dart';
import 'package:pet_diabet/widgets/rounded_input.dart';
import 'package:pet_diabet/widgets/password_field.dart';
import 'package:pet_diabet/widgets/have_account.dart';

enum Usuarios { LoginJogador, LoginMedico }

class BodyLogin extends StatelessWidget {
  final email = TextEditingController();
  final _senha = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/icone.png",
              alignment: Alignment.topCenter,
              height: size.height * 0.35,
            ),

            //SizedBox(height: size.height * 0.03),

            RoundedInputField(
              icon: Icons.person,
              hintText: "Email",
              myController: this.email,
            ),

            RoundedPasswordField(
              myController: this._senha,
            ),

            RoundedButton(
                bord: Colors.lightBlue[700],
                textColor: Colors.white,
                text: "LOGIN",
                press: () => this._eventLogin(context)),

            SizedBox(height: size.height * 0.03),

            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.pushNamed(context, '/signup');
              },
            ),

            SizedBox(height: size.height * 0.05),
            Footer(),
          ],
        ),
      ),
    );
  }

  _eventLogin(context) {
    //print(Usuarios.values.elementAt(0).toString());

    switch(this.email.text){
      case "Jogador":
        return Navigator.pushNamed(context, '/home_page_jogador');

      case "Medico":
        return Navigator.pushNamed(context, '/home_page');

      default:
        return Navigator.pushNamed(context, '/home_page');
    
    }
  }
}
