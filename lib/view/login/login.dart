import 'package:flutter/material.dart';
import 'package:pet_diabet/view/login/components/body_login.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.lightBlue[100],
      //backgroundColor: Color.fromARGB(255, 148,0,211),
      appBar: buildAppBar(context),
      body: BodyLogin(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.lightBlue[200],
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          Icons.keyboard_backspace,
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.more_vert,
            // By default our  icon color is white
            color: Colors.white,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
}
