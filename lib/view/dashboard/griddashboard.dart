import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GridDashboard extends StatelessWidget {
  // Variavel auxiliar para realização de testes de visao do usuario
  // enquanto o app não conecta com a API
  final String type = "Medico";

  _check() {
    List<Items> myList1;

    print("Tipo e" + type);

    if (type == "Medico") {
      myList1 = [item5, item6, item7];
    } else {
      myList1 = [item9, item6, item7];
    }
    return myList1;
  }

  final Items item5 = new Items(
    title: "Pacientes",
    subtitle: "Situações dos Pacientes",
    qtde: "Você possui: 0 Pacientes",
    event: "",
    img: "assets/images/todo.png",
  );

  final Items item6 = new Items(
    title: "Alimentos",
    subtitle: "Cadastrar alimentos",
    qtde: "0 Alimentos",
    event: "/food_register",
    img: "assets/images/food2.png",
  );

  final Items item7 = new Items(
    title: "Configurações",
    subtitle: "",
    qtde: "",
    event: "",
    img: "assets/images/setting3.png",
  );

  final Items item9 = new Items(
    title: "Responsabilidade",
    subtitle: "Situação de seu familiar",
    qtde: "Você possui: 0 cadastrados",
    event: "",
    img: "assets/images/todo.png",
  );

  @override
  Widget build(BuildContext context) {
    List<Items> myList = this._check();

    return Flexible(
      child: GridView.count(
          childAspectRatio: 1.0,
          padding: EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 18,
          mainAxisSpacing: 18,
          children: myList.map((data) {
            return GestureDetector(
                onTap: () => Navigator.pushNamed(context, '/login'),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        data.img,
                        width: 42,
                        colorBlendMode: BlendMode.darken,
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Text(
                        data.title,
                        style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600)),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        data.subtitle,
                        style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                                color: Colors.white38,
                                fontSize: 10,
                                fontWeight: FontWeight.w600)),
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Text(
                        data.qtde,
                        style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                                color: Colors.white70,
                                fontSize: 11,
                                fontWeight: FontWeight.w600)),
                      ),
                    ],
                  ),
                ));
          }).toList()),
    );
  }
}

class Items {
  String title;
  String subtitle;
  String qtde;
  String event;
  String img;
  Items({this.title, this.subtitle, this.qtde, this.event, this.img});
}
