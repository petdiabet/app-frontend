import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_diabet/widgets/footer.dart';
import 'griddashboard.dart';
import 'package:pet_diabet/widgets/social_icon.dart';
import 'package:pet_diabet/widgets/or_divider.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => new HomeState();
}

class HomeState extends State<Home> {
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.lightBlue[100],
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 110,
          ),
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Nome do Usuario",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      "Home",
                      style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Color(0xffa29aac),
                              fontSize: 14,
                              fontWeight: FontWeight.w600)),
                    ),
                  ],
                ),
                IconButton(
                  alignment: Alignment.topCenter,
                  icon: Image.asset(
                    "assets/images/notification.png",
                    width: 24,
                  ),
                  onPressed: () {},
                )
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
         GridDashboard(),
          
          OrDivider(texto: "Siga nossas Redes Sociais"),


          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {},
                  backgroundBorder: Colors.white,
                  svgColor: Colors.lightBlue[900],
                ),
                
                SocalIcon(
                  iconSrc: "assets/icons/twitter.svg",
                  press: () {},
                  backgroundBorder: Colors.white,
                  svgColor: Colors.lightBlue[700],
                ),
                
                SocalIcon(
                  iconSrc: "assets/icons/instagram.svg",
                  press: () {},
                  backgroundBorder: Colors.white,
                  //svgColor: Colors.lightBlue[700],
                ),
              ],
            ),

            SizedBox(height: size.height * 0.05),

            Footer(),
            SizedBox(height: size.height * 0.04),
        ],
      ),
    );
  }
}