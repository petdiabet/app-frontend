
import 'package:flutter/material.dart';
import 'package:pet_diabet/view/choose_pets/option_pets.dart';
import 'package:pet_diabet/view/dashboard/home.dart';
import 'package:pet_diabet/view/food_registration/food_registration.dart';
import 'package:pet_diabet/view/meal/meal_register.dart';
import 'package:pet_diabet/view/home_page/home.dart';
import 'package:pet_diabet/view/login/login.dart';
import 'package:pet_diabet/view/select_options/body.dart';
import 'package:pet_diabet/view/signup/components/body_register.dart';
import 'package:pet_diabet/view/signup/components/register_jogador.dart';
import 'package:pet_diabet/view/signup/components/register_medico.dart';
import 'package:pet_diabet/view/signup/signup.dart';


class Routes {


  static Route<dynamic> generateRoute(RouteSettings settings) {
    
    switch (settings.name) {
      
      case '/':
        return MaterialPageRoute(builder: (_) => Body());
      
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginScreen());

      case '/signup':
        return MaterialPageRoute(builder: (_) => SignUpScreen());

      case '/register':
        return MaterialPageRoute(builder: (_) => Register());    

      case '/register_jogador':
        return MaterialPageRoute(builder: (_) => RegisterJogador());    

      case '/register_medico':
        return MaterialPageRoute(builder: (_) => RegisterMedico());    

      case '/pets':
        return MaterialPageRoute(builder: (_) => OptionsPet());  

      case '/home_page_jogador':
        return MaterialPageRoute(builder: (_) => HomeGamer());
      
      case '/home_page':
        return MaterialPageRoute(builder: (_) => Home());    

      case '/food_register':
        return MaterialPageRoute(builder: (_) => FoodRegister());  

      case '/meal_register':
        return MaterialPageRoute(builder: (_) => MealRegister());      

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
          body: Center(
            child: Text('No route defined for ${settings.name}')
          ),)
        );
    }
  }

}


