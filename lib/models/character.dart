import 'package:flutter/material.dart';

class Character {
  final String name;
  final String imagePath;
  final String description;
  final List<Color> colors;

  Character({this.name, this.imagePath, this.description, this.colors});
}

List characters = [
  Character(
      name: "----",
      imagePath: "assets/images/pets/01_1.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.amber[900], Colors.amber[200], Colors.orange.shade200]),
  Character(
      name: "----",
      imagePath: "assets/images/pets/05.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.deepOrangeAccent[100], Colors.deepOrange[50]]),
  Character(
      name: "----",
      imagePath: "assets/images/pets/02_1.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.orange.shade200, Colors.brown[300]]),
  Character(
      name: "----",
      imagePath: "assets/images/pets/04.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.blue[50], Colors.blue[200]]),
  Character(
      name: "----",
      imagePath: "assets/images/pets/03_1.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.blueGrey.shade200, Colors.lightGreen[900]]),
  Character(
      name: "----",
      imagePath: "assets/images/pets/06_normal.png",
      description:
          "Oi amigo(a), \n\nEstou feliz por você estar aqui, espero que me escolha e cuide bem de mim!",
      colors: [Colors.cyan[600], Colors.white]),
];
