import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:pet_diabet/view/select_options/body.dart';
import 'package:pet_diabet/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Routes.generateRoute,
      
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      
      home: MyHomePage(title: 'PetDiabet'),

    );
  }

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return _introScreen();
  }
}

Widget _introScreen() {
  
  return Stack(
    
    children: <Widget>[
      
      SplashScreen(
        seconds: 3,
        
        gradientBackground: LinearGradient(
          
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          
          colors: [
            Color(0xFF03A9F4),
            Color(0xFFB3E5FC),
            //Color(0xFFB3E5FC)
          ],
        ),
        
        navigateAfterSeconds: Body(),
        loaderColor: Colors.transparent,
      ),
      
      // Setando o icone da logo na tela
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/icons/icone.png"),
            fit: BoxFit.none,
          ),
        ),
      ),

    ],
  );
}

