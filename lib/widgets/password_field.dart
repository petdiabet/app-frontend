import 'package:flutter/material.dart';
import 'text_field.dart';

class RoundedPasswordField extends StatelessWidget {

  final IconData icon;  
  final ValueChanged<String> onChanged;
  final TextEditingController myController;

  const RoundedPasswordField({
    Key key,
    this.icon = Icons.visibility_off,
    this.onChanged,
    this.myController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return TextFieldContainer(
      
      child: TextField(
        
        obscureText: true,
        onChanged: onChanged,
        cursorColor: Colors.blue[50],
        controller: myController,
        
        decoration: InputDecoration(
          
          hintText: "Senha",
          
          icon: Icon(
            Icons.lock,
            color: Colors.black,
          ),
          
          border: InputBorder.none,
        ),
      ),
    );
  }
}