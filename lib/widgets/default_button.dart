
import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key key,
    this.text,
    this.press,
    this.color,
    this.fontSize,
    this.widthButton,
    this.heightButton,
  }) : super(key: key);
  
  final String text;
  final Function press;
  final Object color;
  final double fontSize, widthButton, heightButton;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return SizedBox(
      
      width: screenWidth * widthButton,
      height: screenHeight * this.heightButton,
      
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: color,
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(
            fontSize: screenWidth * this.fontSize,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
