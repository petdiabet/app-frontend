import 'package:flutter/material.dart';

class AlreadyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;

  const AlreadyHaveAnAccountCheck({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Não possui uma conta ? " : "Já possui uma conta ? ",
          style: TextStyle(
            //color: Colors.deepPurple[300]
            color: Colors.indigo[900],
            fontSize: 18,
            fontWeight: FontWeight.w600,
            shadows: [
              Shadow(
                blurRadius: 10.0,
                color: Colors.white,
                offset: Offset(2.0, 3.0),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? " Cadastrar" : "  Login",
            style: TextStyle(
              //color: Colors.deepPurple[300],
              color: Colors.red,
              fontSize: 18,
              fontWeight: FontWeight.bold,

              shadows: [
                Shadow(
                  blurRadius: 10.0,
                  color: Colors.white,
                  offset: Offset(2.0, 3.0),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
