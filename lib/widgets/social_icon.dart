import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SocalIcon extends StatelessWidget {
  final String iconSrc;
  final Color svgColor, backgroundBorder;
  final Function press;

  const SocalIcon({
    Key key,
    this.iconSrc,
    this.press,
    this.svgColor,
    this.backgroundBorder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          //color: Colors.lightBlue[700],
          color: this.backgroundBorder,
          border: Border.all(
            width: 2,
            color: Colors.white,
          ),
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(
          iconSrc,
          height: 30,
          width: 30,
          color: this.svgColor,
        ),
      ),
    );
  }
}
