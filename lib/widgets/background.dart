import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  
  final Widget child;
  
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    Size size = MediaQuery.of(context).size;
    
    return Container(

      decoration: BoxDecoration(
        color: Colors.lightBlue[200],
        //color: Colors.lightBlue[100],
        //color: Color.fromARGB(255,148,0,211)
      ),

      height: size.height,
      width: double.infinity,
      
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          child,
        ],
      ),
    );
  }
}