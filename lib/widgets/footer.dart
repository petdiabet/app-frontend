
import 'package:flutter/material.dart';
import 'package:pet_diabet/config/petdiabet_config.dart';

class Footer extends StatelessWidget {
  
  @override
  Widget build(BuildContext context){

    return Text(
      
      '2020 \u00a9 '+ PetDiabetConfig().name + " - "+ PetDiabetConfig().version,
      //textAlign: ,
      style: TextStyle(
        
        fontSize: 16,
        color: Colors.indigo[900],
        fontWeight: FontWeight.w600,

      ),
      
    );
  }
}
