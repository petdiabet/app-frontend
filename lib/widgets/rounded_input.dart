import 'package:flutter/material.dart';
import 'text_field.dart';

class RoundedInputField extends StatelessWidget {
  
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final TextEditingController myController;

  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon,
    this.onChanged,
    this.myController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return TextFieldContainer(
      
      child: TextField(
        controller: myController,
        onChanged: onChanged,
        cursorColor: Colors.black,
        
        decoration: InputDecoration(
          
          icon: Icon(
            icon,
            color: Colors.black,
          ),
          
          hintText: hintText,
          border: InputBorder.none,
        ),

      ),
    );
  }
}